import { Component, Input } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Items } from '../../providers/providers';

@Component({
  selector: 'product-card',
  templateUrl: 'product-card.html',
  //styleUrls: ['./product-card.scss'],
})
export class ProductCard {
  
  @Input() imageUrl: string;
  @Input() logoUrl: string;
  @Input() title: string;
  @Input() count: number;
  @Input() total: number;
  @Input() small: boolean = false;
  
  public height: number = 240;

  constructor() {

  }
  
  ngOnInit() {
    if(this.small) {
      this.height = 130;
    }
  }

}
